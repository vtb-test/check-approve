"""
The program that merges Merge Requests, if it's been liked not by me
"""
import gitlab
from decouple import config

TOKEN = config('TOKEN')
url = 'https://gitlab.com'
gl = gitlab.Gitlab(url, private_token=TOKEN)
PROJECT_ID = config('PROJECT_ID')

# getting project by ID
project = gl.projects.get(PROJECT_ID)
mrs = project.mergerequests.list()

def merge_rewarded_mr():
    """
    function checks each mr to merge
    """
    mrs = project.mergerequests.list(state='opened')
    for mr in mrs:
        for award_emoji in mr.awardemojis.list(all=True):
            if award_emoji.name == 'thumbsup' and award_emoji.user['id'] != mr.author['id']:
                mr.merge()
            else:
                mr.state_event = 'reopen'
                mr.save()

merge_rewarded_mr()
